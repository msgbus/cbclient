%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 五月 2016 下午4:11
%%%-------------------------------------------------------------------
-author("zhengyinyong").

%%--------------------------------------------------------------------
%% couchbase pool
%%--------------------------------------------------------------------
-define(CB_CLIENT_WORKER_NAME_PREFIX, "cb_client_").
-define(CB_META_POOL, cb_meta_pool).
-define(CB_ALIAS_POOL, cb_alias_pool).
-define(CB_MSG_QUEUE_POOL, cb_msg_queue_pool).
