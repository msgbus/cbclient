%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. 六月 2016 下午3:43
%%%-------------------------------------------------------------------
-module(cbclient_storage).
-author("zhengyinyong").

-include("cbclient.hrl").

%% API
-export([pool_name/1, set/4, set/5, get/3, lenqueue/4, lremove/4, touch/3, rm/2]).

set(Type, Key, Value, TTL) ->
    try
        cberl:set(pool_name(Type),
            cbclient_utils:make_sure_binary(Key), TTL, cbclient_utils:make_sure_binary(Value), transparent),
        ok
    catch
        _Type:Reason  ->
            {error, Reason}
    end.

set(PoolPid, Key, Exp, Value, TranscoderOpts) ->
    cberl:set(PoolPid, cbclient_utils:make_sure_integer(Key), Exp, Value, TranscoderOpts).

get(Type, Key, TranscoderOpts) ->
    Result = try
                 cberl:get(pool_name(Type), cbclient_utils:make_sure_binary(Key), TranscoderOpts)
             catch
                 _Type:Error ->
                     {error, Error}
             end,

    case Result of
        {_, _, Data} ->
            {ok, Data};
        {Key, {error, Reason}} ->
            {error, Reason};
        CBError ->
            {error, CBError}
    end.

lenqueue(Type, Key, Value, MaxLen) ->
    try
        cberl:lenqueue_len(pool_name(Type), cbclient_utils:make_sure_integer(Key), 0, Value, MaxLen)
    catch
        _Type:Reason ->
            {error, Reason}
    end.

lremove(Type, Key, Exp, Value) ->
    cberl:lremove(pool_name(Type), cbclient_utils:make_sure_integer(Key), Exp, cbclient_utils:make_sure_integer(Value)).

touch(Type, Key, TTL) ->
    cberl:touch(pool_name(Type), cbclient_utils:make_sure_integer(Key), TTL).

rm(Type, Key) ->
    try
        cberl:remove(pool_name(Type), cbclient_utils:make_sure_integer(Key))
    catch
        _Type:Error ->
            {error, Error}
    end.

pool_name(Type) ->
    case Type of
        meta ->
            ?CB_META_POOL;
        alias ->
            ?CB_ALIAS_POOL;
        msg_queue ->
            ?CB_MSG_QUEUE_POOL
    end.