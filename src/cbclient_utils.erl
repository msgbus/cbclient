%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. 六月 2016 下午3:39
%%%-------------------------------------------------------------------
-module(cbclient_utils).
-author("zhengyinyong").

%% API
-export([make_sure_binary/1, make_sure_string/1, make_sure_integer/1, make_sure_atom/1, get_env/3]).

make_sure_binary(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

make_sure_string(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
        true ->
            Data
    end.

make_sure_integer(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

make_sure_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

get_env(Applicaiton, Module, Arg) ->
    case application:get_env(Applicaiton, Module) of
        {ok, ArgsLists} ->
            case proplists:get_value(Arg, ArgsLists) of
                undefined ->
                    undefined;
                Value ->
                    Value
            end
    end.