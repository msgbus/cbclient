%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 25. 五月 2016 下午12:16
%%%-------------------------------------------------------------------
-module(cbclient_sup).
-author("zhengyinyong").

-behaviour(supervisor).

-include("cbclient.hrl").

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%% Helper macro for declaring children of supervisor
-define(CHILD(Name, Module, Args, Type), {Name, {Module, start_link, Args}, permanent, 5000, Type, [Module]}).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, {SupFlags :: {RestartStrategy :: supervisor:strategy(),
        MaxR :: non_neg_integer(), MaxT :: non_neg_integer()},
        [ChildSpec :: supervisor:child_spec()]
    }} |
    ignore |
    {error, Reason :: term()}).
init([]) ->
    RestartStrategy = one_for_one,
    MaxRestarts = 1000,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    {ok, {SupFlags, generat_cb_pool_childspec()}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
generat_cb_pool_childspec() ->
    {ok, CBPools} = application:get_env(cbclient, couchbase),
    lists:map(fun({Name, CBArgs}) ->
        PoolName = generate_pool_name(Name),
        WorkerName = generate_worker_name(Name),
        BucketHost = proplists:get_value(bucket_host, CBArgs),
        BucketUserName = proplists:get_value(bucket_username, CBArgs),
        BucketPassword = proplists:get_value(bucket_password, CBArgs),
        BucketName = proplists:get_value(bucket_name, CBArgs),
        ClientConnectionNum = proplists:get_value(client_connection_num, CBArgs),
        Args = [PoolName, ClientConnectionNum, BucketHost, BucketUserName, BucketPassword, BucketName],
        ?CHILD(WorkerName, cberl, Args, worker)
              end, CBPools).

generate_pool_name(Name) ->
    case Name of
        meta ->
            ?CB_META_POOL;
        alias ->
            ?CB_ALIAS_POOL;
        msg_queue ->
            ?CB_MSG_QUEUE_POOL;
        _Else ->
            ignore
    end.

generate_worker_name(Name) ->
    list_to_atom(?CB_CLIENT_WORKER_NAME_PREFIX ++ cbclient_utils:make_sure_string(Name)).