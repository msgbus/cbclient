.PHONY: all deps clean release

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/cbclient

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/cbclient/bin/cbclient start

console: generate
	./rel/cbclient/bin/cbclient console

foreground: generate
	./rel/cbclient/bin/cbclient foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s cbclient
